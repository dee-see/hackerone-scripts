#!/usr/bin/env ruby
# frozen_string_literal: true

require 'json'
require 'csv'

current_year = ARGV[0] || Time.new.year

START_OF_YEAR = "#{current_year}-01-01".freeze
START_OF_NEXT_YEAR = "#{current_year.to_i + 1}-01-01".freeze
API_CREDS = "#{ENV['H1_API_USERNAME']}:#{ENV['H1_API_TOKEN']}".freeze

url = 'https://api.hackerone.com/v1/hackers/payments/payouts'

data = []
loop do
  response = JSON.parse(`curl -s #{url} -u #{API_CREDS}`, symbolize_names: true)
  new_data = response[:data].select do |payout|
    date = payout[:paid_out_at]
    date && date >= START_OF_YEAR && date < START_OF_NEXT_YEAR
  end

  # If we have data but we don't see anything anymore it means we're past the year we're looking for.
  # This assumes reverse chronological order data is returned by the API
  break if new_data.empty? && data.any?

  data.push(*new_data)
  url = response[:links][:next]
  break unless url
end

year_to_date = data.sum { |x| x[:amount] }
puts format('Year to date: %.2f USD', year_to_date)

return unless ARGV[0] == '--csv'

# Dump to CSV for more number crunching!
CSV.open('earnings.csv', 'w') do |csv|
  data.each do |hash|
    csv << hash.values
  end
end
