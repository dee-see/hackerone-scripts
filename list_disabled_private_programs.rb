#!/usr/bin/env ruby
# frozen_string_literal: true

require 'json'

API_CREDS = "#{ENV['H1_API_USERNAME']}:#{ENV['H1_API_TOKEN']}"

url = 'https://api.hackerone.com/v1/hackers/programs'

loop do
  response = JSON.parse(`curl -s #{url} -u #{API_CREDS}`, symbolize_names: true)
  response[:data].map { |p| p[:attributes] }.each do |program|
    next if program[:state] == 'public_mode'

    submission_state = program[:submission_state]
    next if submission_state == 'open'

    puts "https://hackerone.com/#{program[:handle]}?type=team (#{submission_state})"
  end

  url = response[:links][:next]
  break unless url
end
