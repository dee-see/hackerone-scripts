# HackerOne Scripts

This is going to be a collection of scripts I make to automate HackerOne things using their GraphQL API.

## `subscribe_to_programs.rb`

Iterates through all private bug bounty programs and subscribes to notifications (if not already subscribed). If you're interested in notifications for public programs and VDPs, see the comments in the script.

- Requirements: Ruby, `httparty` gem (`gem install httparty`)
- Usage: `./subscribe_to_programs.rb HACKERONE-X-AUTH-TOKEN`

## `list_disabled_private_programs.rb`

Finds private programs that are either paused or disabled so you can leave them and get new invites.

Requirements: Ruby, `httparty` gem (`gem install httparty`)

```shell
$ ./list_disabled_private_programs.rb HACKERONE-X-AUTH-TOKEN
https://hackerone.com/program_name (paused, reports received in the last 90 days: 0)
https://hackerone.com/anoter_program (paused, reports received in the last 90 days: 0)
https://hackerone.com/recently_disabled (disabled, reports received in the last 90 days: 3)
```

## `earnings.rb`

Using the beta (as of this writing) HackerOne API, this script pulls the total earned during the year and optionally dumps a CSV file. This is mostly used for tax reasons. :)

